const express = require("express");
const app = express();
const auth = require("./routes/auth");
const connectDB = require("./database/db");
require("dotenv").config();

//* connect to mongodb
connectDB();

app.use(express.json());
app.use("/api/auth", auth);
app.use("/auth",auth);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.info(`server is running on port ${port}`);
});
